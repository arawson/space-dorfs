
import { MongoClient } from 'mongodb';
import { resolve } from 'path';

//create global variable for mongo database connection

export var client: MongoClient;

export function initConnection() : Promise<void> {
	//force promise to resolve synchronously
	//suck it, js hipsters, i'll wait when i want to
	return MongoClient.connect(process.env.DB_HOST,	{
		promiseLibrary: Promise,
		useNewUrlParser: true
	}).then((value: MongoClient) => {
		client = value;
	});
}
