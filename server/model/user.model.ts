
import * as Joi from 'joi';

export const UserSchema = Joi.object().keys({
	userId: Joi.string().required(),
	nickname: Joi.string().min(3).max(30).required(),
	createdAt: Joi.date(),
	email: Joi.string().email()
});

export class User {
	_id: string;
	userId: String;
	nickname: String;
	createdAt: Date;
	email: String;

	constructor(doc: any) {
		this._id = doc._id;
		this.userId = doc.userId;
		this.nickname = doc.nickname;
		this.createdAt = doc.createdAt;
		this.email = doc.email;
	}
}
