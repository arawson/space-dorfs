
import * as Joi from 'joi';

export class TooMany {
	id: String;

	constructor(id: String) {
		this.id = id;
	}
}

export class NoResults {
	id: String;

	constructor(id: String) {
		this.id = id;
	}
}

export class InvalidResult {
	id: String;
	result: Joi.ValidationError;

	constructor(id: String, result: Joi.ValidationError) {
		this.id = id;
		this.result = result;
	}
}
