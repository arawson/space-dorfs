
import * as express from 'express';
import * as dotenv from 'dotenv';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import { initConnection } from './db';

import * as jwt from 'express-jwt';
import * as jwksRsa from 'jwks-rsa';
const jwtAuthz = require('express-jwt-authz');

import * as homeController from './controllers/home.controller';
import * as userController from './controllers/users.controller';
import { ApplicationRequestHandler } from '../node_modules/@types/express-serve-static-core';

dotenv.config();

const app = express();

/**
 * Allow cross origin requests from the app
 * TODO: move origin to dotenv
 */
const corsOptions = {
	origin: 'http://localhost:3000',
	optionsSuccessStatus: 200
};

/**
 * Authentication middleware. When used, the access token
 * must exist and be verified against the Auth0 JSON web key set
 */
const jwtCheck = jwt({
	//dynamically provide the signing key based on the kind in
	//the header and the signing keys provided by the JWKS endpoint
	secret: jwksRsa.expressJwtSecret({
		cache: true,
		rateLimit: true,
		jwksRequestsPerMinute: 5,
		jwksUri: 'https://aaronmrawson-dev.auth0.com/.well-known/jwks.json'
	}),

	//validate the audience and the issuer
	audience: '1W0cXAnetKYC0HF8YnKr9FiB7q5FcdUa',
	issuer: 'https://aaronmrawson-dev.auth0.com/',
	algorithms: ['RS256']
});

app.set('port', process.env.PORT || 3000);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded( {extended: true} ));
app.use(cors(corsOptions));
app.use(jwtCheck);

// TODO: on production setup, just serve up static files, on dev, we're proxied from ng serve

type RouteFN = (arg1: express.Request, arg2: express.Response) => void;

function loadRoutes(base: string, routes: any, app: express.Application): void {
	for (let key in routes) {
		let route = routes[key];
		if (route.get) {
			console.log('Registering get for route ' + base + key);
			app.get(base + key, route.get);
		}
		if (route.post) {
			console.log('Registering post for route ' + base + key);
			app.post(base + key, route.post);
		}
		if (route.delete) {
			console.log('Registering delete for route ' + base + key);
			app.delete(base + key, route.delete);
		}
	}
}

loadRoutes('/api/test', homeController.routes, app);
loadRoutes('/api/users', userController.routes, app);

initConnection().then(() => {
	app.listen(app.get('port'), () => {
		console.log(('App is running at http://localhost:%d in %s mode'), app.get('port'), app.get('env'));
		console.log('Press CTRL-C to stop\n');
	});
})
.catch((reason: any) => {
	console.log(reason.toString());
	//TODO: also log to file
});

// module.exports = app;
