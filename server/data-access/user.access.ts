
import { UserSchema, User } from '../model/user.model';
import * as Errors from '../model/errors.model';
import { Db, MongoClient } from 'mongodb';
import * as Joi from 'joi';
import { reject, resolve } from '../../node_modules/@types/bluebird';

export class UserAccess {
	db: Db;

	constructor(client: MongoClient) {
		this.db = client.db('user-data');
	}

	byUserId(userId: String) : Promise<User> {
		var col = this.db.collection('user-data')
		return new Promise((resolve, reject) => {
			col.find({userId: userId}).toArray()
			.then((arr: any[]) => {
				if (arr.length > 1) {
					reject(new Errors.TooMany(userId));
					//TODO: output this to the log
				} else if (arr.length < 1) {
					reject(new Errors.NoResults(userId));
					//TODO: output this to the log
				} else {
					const result = Joi.validate(arr[0], UserSchema);

					if (result.error) {
						reject(new Errors.InvalidResult(userId, result.error));
					}

					resolve(new User(arr[0]));
				}
			})
			.catch((reason: any) => {
				return Promise.reject(reason);
			});
		});
	}
}
