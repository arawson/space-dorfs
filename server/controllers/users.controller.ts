
import { Request, Response } from 'express';
import { client } from '../db';
import { UserAccess } from '../data-access/user.access';
import * as Errors from '../model/errors.model';
import { User } from '../model/user.model';
import { InvalidResult } from '../model/errors.model';


/**
 * The users index is initially called by the client to setup the user
 */

export let index = (req: Request, res: Response, next: any) => {
	const PARAM_ID: string = "id";

	if (typeof req.query[PARAM_ID] === "undefined" || req.query[PARAM_ID] === null) {
		res.sendStatus(404);
		next();
		return;
	}

	const id = req.query[PARAM_ID];

	console.log(`/me.get retrieving user: {id: ${req.query[PARAM_ID]}}`);

	const access = new UserAccess(client);

	access.byUserId(req.user.user_id)
	.then((value: User) => {

	})
	.catch((reason: any) => {
		console.log(typeof(reason));
		res.status(500);
		switch(reason.class) {
			case String:
				console.log((<String>reason));

			case Errors.InvalidResult:
				console.log((<InvalidResult>reason).result);
				res.status(500);
		}
	});

	// User.findById(id).then((user: IUserModel) => {
	// 	if (user === null) {
	// 		res.sendStatus(404);
	// 		next();
	// 		return;
	// 	}

	// 	res.json(user);
	// 	next();
	// 	return;
	// })
	// .catch((reason: any) => {
	// 	// TODO: log on failure
	// 	console.log(`Error getting user id:${req.query[PARAM_ID]}: ${reason.toString()}`);
	// 	res.sendStatus(500);
	// 	next();
	// 	return;
	// });

	// console.log(req.user); // this user object is what we need
	res.json({
		ok: true
	});
}

export const routes = {
	"/": {
		"get": index
	}
};
