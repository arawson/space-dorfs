
import { Request, Response } from 'express';
import * as jwt from 'express-jwt';

let pkg = require(__dirname + '/../../package.json');

export let index = (req: Request, res: Response) => {
	console.log(req.user);
	res.json({
		message: 'Welcome to API skele boy.',
		version: pkg.version
	})
}

export const routes = {
	"/": {
		"get": index
	}
};
